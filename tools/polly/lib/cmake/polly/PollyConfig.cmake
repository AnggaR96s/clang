# This file allows users to call find_package(Polly) and pick up our targets.



set(LLVM_VERSION 18.0.0)
find_package(LLVM ${LLVM_VERSION} EXACT REQUIRED CONFIG
             HINTS "/home/angga/tc-build/build/llvm/final/./lib/cmake/llvm")

set(Polly_CMAKE_DIR ${CMAKE_CURRENT_LIST_DIR})
set(Polly_BUNDLED_ISL ON)

set(Polly_DEFINITIONS ${LLVM_DEFINITIONS})
set(Polly_INCLUDE_DIRS /home/angga/tc-build/src/llvm-project/polly/include;/home/angga/tc-build/build/llvm/final/tools/polly/lib/External/isl/include;/home/angga/tc-build/src/llvm-project/polly/lib/External/isl/include;/home/angga/tc-build/build/llvm/final/tools/polly/include ${LLVM_INCLUDE_DIRS})
set(Polly_LIBRARY_DIRS /home/angga/tc-build/build/llvm/final/tools/polly/lib)
set(Polly_EXPORTED_TARGETS Polly;PollyISL;LLVMPolly)
set(Polly_LIBRARIES ${LLVM_LIBRARIES} ${Polly_EXPORTED_TARGETS})

# Imported Targets:

if (NOT TARGET PollyISL)
  add_library(PollyISL STATIC IMPORTED)
endif()

if (NOT TARGET Polly)
  add_library(Polly STATIC IMPORTED)
  set_property(TARGET Polly PROPERTY INTERFACE_LINK_LIBRARIES PollyISL)
endif()

if (NOT TARGET LLVMPolly)
  add_library(LLVMPolly MODULE IMPORTED)
  set_property(TARGET LLVMPolly PROPERTY INTERFACE_LINK_LIBRARIES Polly)
endif()

# Exported locations:
file(GLOB CONFIG_FILES "${Polly_CMAKE_DIR}/PollyExports-*.cmake")
foreach(f ${CONFIG_FILES})
  include(${f})
endforeach()
