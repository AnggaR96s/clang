/*===- TableGen'erated file -------------------------------------*- C++ -*-===*\
|*                                                                            *|
|* Option Parsing Definitions                                                 *|
|*                                                                            *|
|* Automatically generated file, do not edit!                                 *|
|*                                                                            *|
\*===----------------------------------------------------------------------===*/

/////////
// Prefixes

#ifdef PREFIX
#define COMMA ,
PREFIX(prefix_0, {llvm::StringLiteral("")})
PREFIX(prefix_2, {llvm::StringLiteral("-") COMMA llvm::StringLiteral("")})
PREFIX(prefix_1, {llvm::StringLiteral("-") COMMA llvm::StringLiteral("--") COMMA llvm::StringLiteral("")})
#undef COMMA
#endif // PREFIX

/////////
// Prefix Union

#ifdef PREFIX_UNION
#define COMMA ,
PREFIX_UNION({
llvm::StringLiteral("-") COMMA llvm::StringLiteral("--") COMMA llvm::StringLiteral("")})
#undef COMMA
#endif // PREFIX_UNION

/////////
// ValuesCode

#ifdef OPTTABLE_VALUES_CODE
#endif
/////////
// Groups

#ifdef OPTION
OPTION(llvm::ArrayRef<llvm::StringLiteral>(), "Action", action_grp, Group, INVALID, INVALID, nullptr, 0, 0, 0,
       "Action (required)", nullptr, nullptr)
OPTION(llvm::ArrayRef<llvm::StringLiteral>(), "Genric Options", generic_grp, Group, INVALID, INVALID, nullptr, 0, 0, 0,
       "Generic Options", nullptr, nullptr)

//////////
// Options

OPTION(prefix_0, "<input>", INPUT, Input, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_0, "<unknown>", UNKNOWN, Unknown, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "-covered-functions", coveredFunctions, Flag, action_grp, INVALID, nullptr, 0, DefaultVis, 0,
       "Print all covered funcions.", nullptr, nullptr)
OPTION(prefix_2, "-demangle=0", anonymous_3, Flag, INVALID, no_demangle, nullptr, 0, DefaultVis, 0,
       "Alias for --no-demangle", nullptr, nullptr)
OPTION(prefix_1, "-demangle", demangle, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Demangle function names", nullptr, nullptr)
OPTION(prefix_1, "-help", help, Flag, generic_grp, INVALID, nullptr, 0, DefaultVis, 0,
       "Display this help", nullptr, nullptr)
OPTION(prefix_1, "-html-report", htmlReport, Flag, action_grp, INVALID, nullptr, 0, DefaultVis, 0,
       "REMOVED. Use -symbolize & coverage-report-server.py.", nullptr, nullptr)
OPTION(prefix_2, "-h", anonymous_1, Flag, generic_grp, help, nullptr, 0, DefaultVis, 0,
       "Alias for --help", nullptr, nullptr)
OPTION(prefix_1, "-ignorelist=", ignorelist_EQ, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Ignorelist file (sanitizer ignorelist format)", "<string>", nullptr)
OPTION(prefix_1, "-ignorelist", anonymous_6, Separate, INVALID, ignorelist_EQ, nullptr, 0, DefaultVis, 0, nullptr, "<string>", nullptr)
OPTION(prefix_1, "-merge", merge, Flag, action_grp, INVALID, nullptr, 0, DefaultVis, 0,
       "Merges reports.", nullptr, nullptr)
OPTION(prefix_1, "-no-demangle", no_demangle, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Do not demangle function names", nullptr, nullptr)
OPTION(prefix_1, "-no-skip-dead-files", no_skipDeadFiles, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "List dead source files in reports", nullptr, nullptr)
OPTION(prefix_1, "-no-use_default_ignorelist", no_useDefaultIgnoreList, Flag, INVALID, INVALID, nullptr, HelpHidden, DefaultVis, 0,
       "Don't use the default ignore list", nullptr, nullptr)
OPTION(prefix_1, "-not-covered-functions", notCoveredFunctions, Flag, action_grp, INVALID, nullptr, 0, DefaultVis, 0,
       "Print all not covered funcions.", nullptr, nullptr)
OPTION(prefix_1, "-print-coverage-pcs", printCoveragePcs, Flag, action_grp, INVALID, nullptr, 0, DefaultVis, 0,
       "Print coverage instrumentation points addresses.", nullptr, nullptr)
OPTION(prefix_1, "-print-coverage-stats", printCoverageStats, Flag, action_grp, INVALID, nullptr, 0, DefaultVis, 0,
       "Print coverage statistics.", nullptr, nullptr)
OPTION(prefix_1, "-print", print, Flag, action_grp, INVALID, nullptr, 0, DefaultVis, 0,
       "Print coverage addresses", nullptr, nullptr)
OPTION(prefix_2, "-skip-dead-files=0", anonymous_4, Flag, INVALID, no_skipDeadFiles, nullptr, 0, DefaultVis, 0,
       "Alias for --no-skip-dead-files", nullptr, nullptr)
OPTION(prefix_1, "-skip-dead-files", skipDeadFiles, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Do not list dead source files in reports", nullptr, nullptr)
OPTION(prefix_1, "-strip_path_prefix=", stripPathPrefix_EQ, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Strip this prefix from files paths in reports", "<string>", nullptr)
OPTION(prefix_1, "-strip_path_prefix", anonymous_0, Separate, INVALID, stripPathPrefix_EQ, nullptr, 0, DefaultVis, 0, nullptr, "<string>", nullptr)
OPTION(prefix_1, "-symbolize", symbolize, Flag, action_grp, INVALID, nullptr, 0, DefaultVis, 0,
       "Produces a symbolized JSON report from binary report.", nullptr, nullptr)
OPTION(prefix_2, "-use_default_ignorelist=0", anonymous_5, Flag, INVALID, no_useDefaultIgnoreList, nullptr, 0, DefaultVis, 0,
       "Alias for --no-use_default_ignore_list", nullptr, nullptr)
OPTION(prefix_1, "-use_default_ignorelist", useDefaultIgnoreList, Flag, INVALID, INVALID, nullptr, HelpHidden, DefaultVis, 0,
       "Use the default ignore list", nullptr, nullptr)
OPTION(prefix_1, "-version", version, Flag, generic_grp, INVALID, nullptr, 0, DefaultVis, 0,
       "Display the version", nullptr, nullptr)
OPTION(prefix_2, "-v", anonymous_2, Flag, generic_grp, version, nullptr, 0, DefaultVis, 0,
       "Alias for --version", nullptr, nullptr)
#endif // OPTION

#ifdef SIMPLE_ENUM_VALUE_TABLE

struct SimpleEnumValue {
  const char *Name;
  unsigned Value;
};

struct SimpleEnumValueTable {
  const SimpleEnumValue *Table;
  unsigned Size;
};
static const SimpleEnumValueTable SimpleEnumValueTables[] = {};
static const unsigned SimpleEnumValueTablesSize = std::size(SimpleEnumValueTables);
#endif // SIMPLE_ENUM_VALUE_TABLE


