/*===- TableGen'erated file -------------------------------------*- C++ -*-===*\
|*                                                                            *|
|* Option Parsing Definitions                                                 *|
|*                                                                            *|
|* Automatically generated file, do not edit!                                 *|
|*                                                                            *|
\*===----------------------------------------------------------------------===*/

/////////
// Prefixes

#ifdef PREFIX
#define COMMA ,
PREFIX(prefix_0, {llvm::StringLiteral("")})
PREFIX(prefix_2, {llvm::StringLiteral("--") COMMA llvm::StringLiteral("-") COMMA llvm::StringLiteral("")})
PREFIX(prefix_1, {llvm::StringLiteral("/") COMMA llvm::StringLiteral("-") COMMA llvm::StringLiteral("")})
#undef COMMA
#endif // PREFIX

/////////
// Prefix Union

#ifdef PREFIX_UNION
#define COMMA ,
PREFIX_UNION({
llvm::StringLiteral("-") COMMA llvm::StringLiteral("--") COMMA llvm::StringLiteral("/") COMMA llvm::StringLiteral("")})
#undef COMMA
#endif // PREFIX_UNION

/////////
// ValuesCode

#ifdef OPTTABLE_VALUES_CODE
#endif
/////////
// Groups

#ifdef OPTION
OPTION(llvm::ArrayRef<llvm::StringLiteral>(), "<ml options>", ml_Group, Group, INVALID, INVALID, nullptr, 0, 0, 0,
       "ML.EXE COMPATIBILITY OPTIONS", nullptr, nullptr)
OPTION(llvm::ArrayRef<llvm::StringLiteral>(), "unsupported", unsupported_Group, Group, INVALID, INVALID, nullptr, 0, 0, 0,
       "UNSUPPORTED ML.EXE COMPATIBILITY OPTIONS", nullptr, nullptr)

//////////
// Options

OPTION(prefix_0, "<input>", INPUT, Input, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_0, "<unknown>", UNKNOWN, Unknown, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/?", help, Flag, ml_Group, INVALID, nullptr, 0, DefaultVis, 0,
       "Display available options", nullptr, nullptr)
OPTION(prefix_2, "--as-lex", as_lex, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Lex tokens from a file without assembling", nullptr, nullptr)
OPTION(prefix_1, "/AT", tiny_model_support, Flag, unsupported_Group, INVALID, nullptr, HelpHidden, DefaultVis, 0,
       "", nullptr, nullptr)
OPTION(prefix_1, "/Bl", alternate_linker, Joined, unsupported_Group, INVALID, nullptr, HelpHidden, DefaultVis, 0,
       "", nullptr, nullptr)
OPTION(prefix_1, "/coff", coff_object_file, Flag, unsupported_Group, INVALID, nullptr, HelpHidden, DefaultVis, 0,
       "", nullptr, nullptr)
OPTION(prefix_1, "/Cp", preserve_identifier_case, Flag, unsupported_Group, INVALID, nullptr, HelpHidden, DefaultVis, 0,
       "", nullptr, nullptr)
OPTION(prefix_1, "/Cu", uppercase_identifiers, Flag, unsupported_Group, INVALID, nullptr, HelpHidden, DefaultVis, 0,
       "", nullptr, nullptr)
OPTION(prefix_1, "/Cx", preserve_extern_case, Flag, unsupported_Group, INVALID, nullptr, HelpHidden, DefaultVis, 0,
       "", nullptr, nullptr)
OPTION(prefix_1, "/c", assemble_only, Flag, ml_Group, INVALID, nullptr, 0, DefaultVis, 0,
       "Assemble only; do not link", nullptr, nullptr)
OPTION(prefix_2, "--debug-only=", debug_only, CommaJoined, INVALID, INVALID, nullptr, HelpHidden, DefaultVis, 0,
       "Enable a specific type of debug output (comma separated list of types)", nullptr, nullptr)
OPTION(prefix_2, "--debug", debug, Flag, INVALID, INVALID, nullptr, HelpHidden, DefaultVis, 0,
       "Enable debug output", nullptr, nullptr)
OPTION(prefix_1, "/D", define, JoinedOrSeparate, ml_Group, INVALID, nullptr, 0, DefaultVis, 0,
       "Define <macro> to <value> (or blank if <value> omitted)", "<macro>=<value>", nullptr)
OPTION(prefix_1, "/EP", output_preprocessed, Flag, unsupported_Group, INVALID, nullptr, HelpHidden, DefaultVis, 0,
       "", nullptr, nullptr)
OPTION(prefix_1, "/ERRORREPORT", errorreport, Joined, unsupported_Group, INVALID, nullptr, HelpHidden, DefaultVis, 0,
       "", nullptr, nullptr)
OPTION(prefix_2, "--fatal-warnings", fatal_warnings, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Treat warnings as errors", nullptr, nullptr)
OPTION(prefix_1, "/Fe", executable_file, Separate, unsupported_Group, INVALID, nullptr, HelpHidden, DefaultVis, 0,
       "", nullptr, nullptr)
OPTION(prefix_2, "--filetype=", filetype, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Emit a file with the given type", nullptr, "obj,s,null")
OPTION(prefix_1, "/FI", code_listing_file, Joined, unsupported_Group, INVALID, nullptr, HelpHidden, DefaultVis, 0,
       "", nullptr, nullptr)
OPTION(prefix_1, "/Fm", linker_map_file, Joined, unsupported_Group, INVALID, nullptr, HelpHidden, DefaultVis, 0,
       "", nullptr, nullptr)
OPTION(prefix_1, "/Fo", output_file, JoinedOrSeparate, ml_Group, INVALID, nullptr, 0, DefaultVis, 0,
       "Names the output file", nullptr, nullptr)
OPTION(prefix_1, "/FPi", fp_emulator_fixups, Flag, unsupported_Group, INVALID, nullptr, HelpHidden, DefaultVis, 0,
       "", nullptr, nullptr)
OPTION(prefix_1, "/FR", extended_source_browser_file, Joined, unsupported_Group, INVALID, nullptr, HelpHidden, DefaultVis, 0,
       "", nullptr, nullptr)
OPTION(prefix_1, "/Fr", source_browser_file, Joined, unsupported_Group, INVALID, nullptr, HelpHidden, DefaultVis, 0,
       "", nullptr, nullptr)
OPTION(prefix_1, "/F", stacksize, Separate, unsupported_Group, INVALID, nullptr, HelpHidden, DefaultVis, 0,
       "", nullptr, nullptr)
OPTION(prefix_1, "/Gc", pascal_conventions, Flag, unsupported_Group, INVALID, nullptr, HelpHidden, DefaultVis, 0,
       "", nullptr, nullptr)
OPTION(prefix_1, "/Gd", c_conventions, Flag, unsupported_Group, INVALID, nullptr, HelpHidden, DefaultVis, 0,
       "", nullptr, nullptr)
OPTION(prefix_2, "--gmtime", gmtime, Flag, INVALID, utc, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/GZ", stdcall_conventions, Flag, unsupported_Group, INVALID, nullptr, HelpHidden, DefaultVis, 0,
       "", nullptr, nullptr)
OPTION(prefix_1, "/help", help_long, Flag, ml_Group, help, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/H", extern_name_limit, Separate, unsupported_Group, INVALID, nullptr, HelpHidden, DefaultVis, 0,
       "", nullptr, nullptr)
OPTION(prefix_1, "/I", include_path, JoinedOrSeparate, ml_Group, INVALID, nullptr, 0, DefaultVis, 0,
       "Sets path for include files", nullptr, nullptr)
OPTION(prefix_2, "--m", bitness, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Target platform (x86 or x86-64)", nullptr, "32,64")
OPTION(prefix_1, "/nologo", no_logo, Flag, ml_Group, INVALID, nullptr, 0, DefaultVis, 0,
       "", nullptr, nullptr)
OPTION(prefix_1, "/omf", omf_object_file, Flag, unsupported_Group, INVALID, nullptr, HelpHidden, DefaultVis, 0,
       "", nullptr, nullptr)
OPTION(prefix_2, "--output-att-asm", output_att_asm, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Use ATT syntax for output assembly", nullptr, nullptr)
OPTION(prefix_2, "--preserve-comments", preserve_comments, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Preserve comments in output assembly", nullptr, nullptr)
OPTION(prefix_2, "--print-imm-hex", print_imm_hex, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Prefer hex format for immediate values in output assembly", nullptr, nullptr)
OPTION(prefix_1, "/safeseh", safeseh, Flag, ml_Group, INVALID, nullptr, 0, DefaultVis, 0,
       "Mark resulting object files as either containing no exception handlers or containing exception handlers that are all declared with .SAFESEH. Only available in 32-bit.", nullptr, nullptr)
OPTION(prefix_2, "--save-temp-labels", save_temp_labels, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Don't discard temporary labels", nullptr, nullptr)
OPTION(prefix_1, "/Sa", full_listing, Flag, unsupported_Group, INVALID, nullptr, HelpHidden, DefaultVis, 0,
       "", nullptr, nullptr)
OPTION(prefix_1, "/Sf", first_pass_listing, Flag, unsupported_Group, INVALID, nullptr, HelpHidden, DefaultVis, 0,
       "", nullptr, nullptr)
OPTION(prefix_2, "--show-encoding", show_encoding, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Show instruction encodings in output assembly", nullptr, nullptr)
OPTION(prefix_2, "--show-inst-operands", show_inst_operands, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Show instructions operands as parsed", nullptr, nullptr)
OPTION(prefix_2, "--show-inst", show_inst, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Show internal instruction representation in output assembly", nullptr, nullptr)
OPTION(prefix_1, "/SI", listing_width, Separate, unsupported_Group, INVALID, nullptr, HelpHidden, DefaultVis, 0,
       "", nullptr, nullptr)
OPTION(prefix_1, "/Sn", listing_without_symbols, Flag, unsupported_Group, INVALID, nullptr, HelpHidden, DefaultVis, 0,
       "", nullptr, nullptr)
OPTION(prefix_1, "/Sp", listing_page_length, Separate, unsupported_Group, INVALID, nullptr, HelpHidden, DefaultVis, 0,
       "", nullptr, nullptr)
OPTION(prefix_1, "/Ss", listing_subtitle, Separate, unsupported_Group, INVALID, nullptr, HelpHidden, DefaultVis, 0,
       "", nullptr, nullptr)
OPTION(prefix_1, "/St", listing_title, Separate, unsupported_Group, INVALID, nullptr, HelpHidden, DefaultVis, 0,
       "", nullptr, nullptr)
OPTION(prefix_1, "/Sx", listing_false_conditionals, Flag, unsupported_Group, INVALID, nullptr, HelpHidden, DefaultVis, 0,
       "", nullptr, nullptr)
OPTION(prefix_1, "/Ta", assembly_file, JoinedOrSeparate, ml_Group, INVALID, nullptr, 0, DefaultVis, 0,
       "Assemble source file with the given name. Used if the filename begins with a forward slash.", nullptr, nullptr)
OPTION(prefix_2, "--timestamp=", timestamp, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Specify the assembly timestamp (used for @Date and @Time built-ins)", nullptr, nullptr)
OPTION(prefix_2, "--utc", utc, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Render @Date and @Time built-ins in GMT/UTC", nullptr, nullptr)
OPTION(prefix_1, "/WX", error_on_warning, Flag, ml_Group, fatal_warnings, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/W", warning_level, Joined, unsupported_Group, INVALID, nullptr, HelpHidden, DefaultVis, 0,
       "", nullptr, nullptr)
OPTION(prefix_1, "/w", extra_warnings, Flag, unsupported_Group, INVALID, nullptr, HelpHidden, DefaultVis, 0,
       "", nullptr, nullptr)
OPTION(prefix_1, "/X", ignore_include_envvar, Flag, ml_Group, INVALID, nullptr, 0, DefaultVis, 0,
       "Ignore the INCLUDE environment variable", nullptr, nullptr)
OPTION(prefix_1, "/Zd", line_number_info, Flag, unsupported_Group, INVALID, nullptr, HelpHidden, DefaultVis, 0,
       "", nullptr, nullptr)
OPTION(prefix_1, "/Zf", export_all_symbols, Flag, unsupported_Group, INVALID, nullptr, HelpHidden, DefaultVis, 0,
       "", nullptr, nullptr)
OPTION(prefix_1, "/Zi", codeview_info, Flag, unsupported_Group, INVALID, nullptr, HelpHidden, DefaultVis, 0,
       "", nullptr, nullptr)
OPTION(prefix_1, "/Zm", enable_m510_option, Flag, unsupported_Group, INVALID, nullptr, HelpHidden, DefaultVis, 0,
       "", nullptr, nullptr)
OPTION(prefix_1, "/Zp", structure_packing, Joined, unsupported_Group, INVALID, nullptr, HelpHidden, DefaultVis, 0,
       "", nullptr, nullptr)
OPTION(prefix_1, "/Zs", parse_only, Flag, ml_Group, filetype, "null\0", 0, DefaultVis, 0,
       "Run a syntax-check only", nullptr, nullptr)
#endif // OPTION

#ifdef SIMPLE_ENUM_VALUE_TABLE

struct SimpleEnumValue {
  const char *Name;
  unsigned Value;
};

struct SimpleEnumValueTable {
  const SimpleEnumValue *Table;
  unsigned Size;
};
static const SimpleEnumValueTable SimpleEnumValueTables[] = {};
static const unsigned SimpleEnumValueTablesSize = std::size(SimpleEnumValueTables);
#endif // SIMPLE_ENUM_VALUE_TABLE


