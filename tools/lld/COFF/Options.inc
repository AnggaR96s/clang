/*===- TableGen'erated file -------------------------------------*- C++ -*-===*\
|*                                                                            *|
|* Option Parsing Definitions                                                 *|
|*                                                                            *|
|* Automatically generated file, do not edit!                                 *|
|*                                                                            *|
\*===----------------------------------------------------------------------===*/

/////////
// Prefixes

#ifdef PREFIX
#define COMMA ,
PREFIX(prefix_0, {llvm::StringLiteral("")})
PREFIX(prefix_2, {llvm::StringLiteral("--") COMMA llvm::StringLiteral("")})
PREFIX(prefix_1, {llvm::StringLiteral("/") COMMA llvm::StringLiteral("-") COMMA llvm::StringLiteral("/?") COMMA llvm::StringLiteral("-?") COMMA llvm::StringLiteral("")})
PREFIX(prefix_3, {llvm::StringLiteral("/??") COMMA llvm::StringLiteral("-??") COMMA llvm::StringLiteral("/?") COMMA llvm::StringLiteral("-?") COMMA llvm::StringLiteral("")})
#undef COMMA
#endif // PREFIX

/////////
// Prefix Union

#ifdef PREFIX_UNION
#define COMMA ,
PREFIX_UNION({
llvm::StringLiteral("-") COMMA llvm::StringLiteral("--") COMMA llvm::StringLiteral("-?") COMMA llvm::StringLiteral("-??") COMMA llvm::StringLiteral("/") COMMA llvm::StringLiteral("/?") COMMA llvm::StringLiteral("/??") COMMA llvm::StringLiteral("")})
#undef COMMA
#endif // PREFIX_UNION

/////////
// ValuesCode

#ifdef OPTTABLE_VALUES_CODE
#endif
/////////
// Groups

#ifdef OPTION

//////////
// Options

OPTION(prefix_0, "<input>", INPUT, Input, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_0, "<unknown>", UNKNOWN, Unknown, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/align:", align, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Section alignment", nullptr, nullptr)
OPTION(prefix_1, "/aligncomm:", aligncomm, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Set common symbol alignment", nullptr, nullptr)
OPTION(prefix_1, "/allowbind:no", allowbind_no, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Disable DLL binding", nullptr, nullptr)
OPTION(prefix_1, "/allowbind", allowbind, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Enable DLL binding (default)", nullptr, nullptr)
OPTION(prefix_1, "/allowisolation:no", allowisolation_no, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Disable DLL isolation", nullptr, nullptr)
OPTION(prefix_1, "/allowisolation", allowisolation, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Enable DLL isolation (default)", nullptr, nullptr)
OPTION(prefix_1, "/alternatename:", alternatename, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Define weak alias", nullptr, nullptr)
OPTION(prefix_1, "/appcontainer:no", appcontainer_no, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Image can run outside an app container (default)", nullptr, nullptr)
OPTION(prefix_1, "/appcontainer", appcontainer, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Image can only be run in an app container", nullptr, nullptr)
OPTION(prefix_1, "/assemblydebug:", assemblydebug_opt, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/assemblydebug", assemblydebug, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/auto-import:no", auto_import_no, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/auto-import", auto_import, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/base:", base, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Base address of the program", nullptr, nullptr)
OPTION(prefix_1, "/Brepro", repro, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Use a hash of the executable as the PE header timestamp", nullptr, nullptr)
OPTION(prefix_1, "/call-graph-ordering-file:", call_graph_ordering_file, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Layout sections to optimize the given callgraph", nullptr, nullptr)
OPTION(prefix_1, "/call-graph-profile-sort:no", call_graph_profile_sort_no, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Do not reorder sections with call graph profile", nullptr, nullptr)
OPTION(prefix_1, "/call-graph-profile-sort", call_graph_profile_sort, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Reorder sections with call graph profile (default)", nullptr, nullptr)
OPTION(prefix_1, "/cetcompat:no", cetcompat_no, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Don't mark executable image as compatible with Control-flow Enforcement Technology (CET) Shadow Stack (default)", nullptr, nullptr)
OPTION(prefix_1, "/cetcompat", cetcompat, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Mark executable image as compatible with Control-flow Enforcement Technology (CET) Shadow Stack", nullptr, nullptr)
OPTION(prefix_2, "--color-diagnostics=", color_diagnostics_eq, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Use colors in diagnostics (default: auto)", "[auto,always,never]", nullptr)
OPTION(prefix_2, "--color-diagnostics", color_diagnostics, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Alias for --color-diagnostics=always", nullptr, nullptr)
OPTION(prefix_1, "/debug:", debug_opt, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Embed a symbol table in the image with option", nullptr, nullptr)
OPTION(prefix_1, "/debugtype:", debugtype, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Debug Info Options", nullptr, nullptr)
OPTION(prefix_1, "/debug", debug, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Embed a symbol table in the image", nullptr, nullptr)
OPTION(prefix_1, "/def:", deffile, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Use module-definition file", nullptr, nullptr)
OPTION(prefix_1, "/defaultlib:", defaultlib, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Add the library to the list of input files", nullptr, nullptr)
OPTION(prefix_1, "/delay:", delay, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/delayload:", delayload, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Delay loaded DLL name", nullptr, nullptr)
OPTION(prefix_1, "/demangle:no", demangle_no, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Do not demangle symbols in output", nullptr, nullptr)
OPTION(prefix_1, "/demangle", demangle, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Demangle symbols in output (default)", nullptr, nullptr)
OPTION(prefix_1, "/diasdkdir:", diasdkdir, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Set the location of the DIA SDK", nullptr, nullptr)
OPTION(prefix_1, "/disallowlib:", disallowlib, Joined, INVALID, nodefaultlib, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/dll", dll, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Create a DLL", nullptr, nullptr)
OPTION(prefix_1, "/driver:uponly,wdm", driver_uponly_wdm, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/driver:uponly", driver_uponly, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Set IMAGE_DLL_CHARACTERISTICS_WDM_DRIVER bit in PE header", nullptr, nullptr)
OPTION(prefix_1, "/driver:wdm,uponly", driver_wdm_uponly, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/driver:wdm", driver_wdm, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Set IMAGE_FILE_UP_SYSTEM_ONLY bit in PE header", nullptr, nullptr)
OPTION(prefix_1, "/driver", driver, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Generate a Windows NT Kernel Mode Driver", nullptr, nullptr)
OPTION(prefix_1, "/dwodir:", dwodir, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Directory to store .dwo files when LTO and debug fission are used", nullptr, nullptr)
OPTION(prefix_1, "/dynamicbase:no", dynamicbase_no, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Disable ASLR (default when /fixed)", nullptr, nullptr)
OPTION(prefix_1, "/dynamicbase", dynamicbase, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Enable ASLR (default unless /fixed)", nullptr, nullptr)
OPTION(prefix_1, "/editandcontinue", editandcontinue, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/emitpogophaseinfo", emitpogophaseinfo, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/end-lib", end_lib, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "End group of objects treated as if they were in a library", nullptr, nullptr)
OPTION(prefix_1, "/entry:", entry, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Name of entry point symbol", nullptr, nullptr)
OPTION(prefix_1, "/errorlimit:", errorlimit, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Maximum number of errors to emit before stopping (0 = no limit)", nullptr, nullptr)
OPTION(prefix_1, "/errorreport:", errorreport, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/exclude-all-symbols", exclude_all_symbols, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/exclude-symbols:", exclude_symbols, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Exclude symbols from automatic export", "<symbol[,symbol,...]>", nullptr)
OPTION(prefix_1, "/export-all-symbols", export_all_symbols, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/export:", export, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Export a function", nullptr, nullptr)
OPTION(prefix_1, "/failifmismatch:", failifmismatch, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "", nullptr, nullptr)
OPTION(prefix_1, "/fastfail", fastfail, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/filealign:", filealign, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Section alignment in the output file", nullptr, nullptr)
OPTION(prefix_1, "/fixed:no", fixed_no, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Enable base relocations (default)", nullptr, nullptr)
OPTION(prefix_1, "/fixed", fixed, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Disable base relocations", nullptr, nullptr)
OPTION(prefix_1, "/force:multipleres", force_multipleres, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Allow multiply defined resources when creating executables", nullptr, nullptr)
OPTION(prefix_1, "/force:multiple", force_multiple, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Allow multiply defined symbols when creating executables", nullptr, nullptr)
OPTION(prefix_1, "/force:unresolved", force_unresolved, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Allow undefined symbols when creating executables", nullptr, nullptr)
OPTION(prefix_1, "/force", force, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Allow undefined and multiply defined symbols", nullptr, nullptr)
OPTION(prefix_1, "/functionpadmin:", functionpadmin_opt, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Prepares an image for hotpatching", nullptr, nullptr)
OPTION(prefix_1, "/functionpadmin", functionpadmin, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/guard:", guard, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Control flow guard", nullptr, nullptr)
OPTION(prefix_1, "/guardsym:", guardsym, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/heap:", heap, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Size of the heap", nullptr, nullptr)
OPTION(prefix_1, "/help", help, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/highentropyva:no", highentropyva_no, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Disable 64-bit ASLR", nullptr, nullptr)
OPTION(prefix_1, "/highentropyva", highentropyva, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Enable 64-bit ASLR (default on 64-bit)", nullptr, nullptr)
OPTION(prefix_1, "/idlout:", idlout, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/ignore:", ignore, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Specify warning codes to ignore", nullptr, nullptr)
OPTION(prefix_1, "/ignoreidl", ignoreidl, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/ilk:", ilk, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/implib:", implib, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Import library name", nullptr, nullptr)
OPTION(prefix_1, "/include:", incl, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Force symbol to be added to symbol table as undefined one", nullptr, nullptr)
OPTION(prefix_1, "/includeoptional:", include_optional, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Add symbol as undefined, but allow it to remain undefined", nullptr, nullptr)
OPTION(prefix_1, "/incremental:no", incremental_no, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Overwrite import library even if contents are unchanged", nullptr, nullptr)
OPTION(prefix_1, "/incremental", incremental, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Keep original import library if contents are unchanged", nullptr, nullptr)
OPTION(prefix_1, "/inferasanlibs:no", inferasanlibs_no, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "No effect (default)", nullptr, nullptr)
OPTION(prefix_1, "/inferasanlibs", inferasanlibs, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Unused, generates a warning", nullptr, nullptr)
OPTION(prefix_1, "/integritycheck:no", integritycheck_no, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "No effect (default)", nullptr, nullptr)
OPTION(prefix_1, "/integritycheck", integritycheck, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Set FORCE_INTEGRITY bit in PE header", nullptr, nullptr)
OPTION(prefix_1, "/kernel", kernel, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/kill-at", kill_at, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/largeaddressaware:no", largeaddressaware_no, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Disable large addresses (default on 32-bit)", nullptr, nullptr)
OPTION(prefix_1, "/largeaddressaware", largeaddressaware, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Enable large addresses (default on 64-bit)", nullptr, nullptr)
OPTION(prefix_1, "/libpath:", libpath, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Additional library search path", nullptr, nullptr)
OPTION(prefix_1, "/lib", lib, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Act like lib.exe; must be first argument if present", nullptr, nullptr)
OPTION(prefix_1, "/linkrepro:", linkrepro, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Write repro.tar containing inputs and command to reproduce link", "directory", nullptr)
OPTION(prefix_1, "/lldemit:", lldemit, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Specify output type", nullptr, nullptr)
OPTION(prefix_1, "/lldignoreenv", lldignoreenv, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Ignore environment variables like %LIB%", nullptr, nullptr)
OPTION(prefix_1, "/lldltocache:", lldltocache, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Path to ThinLTO cached object file directory", nullptr, nullptr)
OPTION(prefix_1, "/lldltocachepolicy:", lldltocachepolicy, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Pruning policy for the ThinLTO cache", nullptr, nullptr)
OPTION(prefix_1, "/lldmap:", lldmap_file, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/lldmap", lldmap, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/lldmingw", lldmingw, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/lldsavetemps", lldsavetemps, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Save intermediate LTO compilation results", nullptr, nullptr)
OPTION(prefix_1, "/ltcg:", ltcg_opt, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/ltcgout:", ltcgout, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/ltcg", ltcg, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/lto-cs-profile-file:", lto_cs_profile_file, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Context sensitive profile file path", nullptr, nullptr)
OPTION(prefix_1, "/lto-cs-profile-generate", lto_cs_profile_generate, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Perform context sensitive PGO instrumentation", nullptr, nullptr)
OPTION(prefix_1, "/lto-obj-path:", lto_obj_path, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "output native object for merged LTO unit to this path", nullptr, nullptr)
OPTION(prefix_1, "/lto-pgo-warn-mismatch:no", lto_pgo_warn_mismatch_no, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "turn off warnings about profile cfg mismatch", nullptr, nullptr)
OPTION(prefix_1, "/lto-pgo-warn-mismatch", lto_pgo_warn_mismatch, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "turn on warnings about profile cfg mismatch (default)>", nullptr, nullptr)
OPTION(prefix_1, "/machine:", machine, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Specify target platform", nullptr, nullptr)
OPTION(prefix_1, "/manifest:", manifest_colon, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "NO disables manifest output; EMBED[,ID=#] embeds manifest as resource in the image", nullptr, nullptr)
OPTION(prefix_1, "/manifestdependency:", manifestdependency, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Attributes for <dependency> element in manifest file; implies /manifest", nullptr, nullptr)
OPTION(prefix_1, "/manifestfile:", manifestfile, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Manifest output path, with /manifest", nullptr, nullptr)
OPTION(prefix_1, "/manifestinput:", manifestinput, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Additional manifest inputs; only valid with /manifest:embed", nullptr, nullptr)
OPTION(prefix_1, "/manifestuac:", manifestuac, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "User access control", nullptr, nullptr)
OPTION(prefix_1, "/manifest", manifest, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Create .manifest file", nullptr, nullptr)
OPTION(prefix_1, "/map:", map_file, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/mapinfo:", map_info, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Include the specified information in a map file", nullptr, nullptr)
OPTION(prefix_1, "/map", map, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/maxilksize:", maxilksize, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/merge:", merge, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Combine sections", nullptr, nullptr)
OPTION(prefix_1, "/mllvm:", mllvm, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Options to pass to LLVM", nullptr, nullptr)
OPTION(prefix_1, "/natvis:", natvis, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Path to natvis file to embed in the PDB", nullptr, nullptr)
OPTION(prefix_2, "--no-color-diagnostics", no_color_diagnostics, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Alias for --color-diagnostics=never", nullptr, nullptr)
OPTION(prefix_1, "/nodefaultlib:", nodefaultlib, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Remove a default library", nullptr, nullptr)
OPTION(prefix_1, "/nodefaultlib", nodefaultlib_all, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Remove all default libraries", nullptr, nullptr)
OPTION(prefix_1, "/noentry", noentry, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Don't add reference to DllMainCRTStartup; only valid with /dll", nullptr, nullptr)
OPTION(prefix_1, "/noimplib", noimplib, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Don't output an import lib", nullptr, nullptr)
OPTION(prefix_1, "/nologo", nologo, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/noseh", noseh, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/nxcompat:no", nxcompat_no, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Disable data execution provention", nullptr, nullptr)
OPTION(prefix_1, "/nxcompat", nxcompat, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Enable data execution prevention (default)", nullptr, nullptr)
OPTION(prefix_1, "/opt:", opt, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Control optimizations", nullptr, nullptr)
OPTION(prefix_1, "/order:", order, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Put functions in order", nullptr, nullptr)
OPTION(prefix_1, "/osversion:", osversion, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/out:", out, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Path to file to write output", nullptr, nullptr)
OPTION(prefix_1, "/output-def:", output_def, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/pdb:", pdb, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "PDB file path", nullptr, nullptr)
OPTION(prefix_1, "/pdbaltpath:", pdbaltpath, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "PDB file path to embed in the image", nullptr, nullptr)
OPTION(prefix_1, "/pdbcompress", pdbcompress, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/pdbpagesize:", pdbpagesize, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "PDB page size", nullptr, nullptr)
OPTION(prefix_1, "/pdbsourcepath:", pdb_source_path, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Base path used to make relative source file path absolute in PDB", nullptr, nullptr)
OPTION(prefix_1, "/pdbstream:", pdbstream, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Embed the contents of <file> in the PDB as named stream <name>", "<name>=<file>", nullptr)
OPTION(prefix_1, "/pdbstripped:", pdbstripped, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Stripped PDB file path", nullptr, nullptr)
OPTION(prefix_1, "/print-search-paths", print_search_paths, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/print-symbol-order:", print_symbol_order, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Print a symbol order specified by /call-graph-ordering-file and /call-graph-profile-sort into the specified file", nullptr, nullptr)
OPTION(prefix_1, "/profile", profile, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/release", release, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Set the Checksum in the header of an PE file", nullptr, nullptr)
OPTION(prefix_1, "/reproduce:", reproduce, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Write tar file containing inputs and command to reproduce link", "filename", nullptr)
OPTION(prefix_2, "--rsp-quoting=", rsp_quoting, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Quoting style for response files, 'windows' (default) or 'posix'", nullptr, nullptr)
OPTION(prefix_1, "/runtime-pseudo-reloc:no", runtime_pseudo_reloc_no, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/runtime-pseudo-reloc", runtime_pseudo_reloc, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/safeseh:no", safeseh_no, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Don't produce an image with Safe Exception Handler", nullptr, nullptr)
OPTION(prefix_1, "/safeseh", safeseh, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Produce an image with Safe Exception Handler (only for x86)", nullptr, nullptr)
OPTION(prefix_1, "/section:", section, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Specify section attributes", nullptr, nullptr)
OPTION(prefix_1, "/stack:", stack, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Size of the stack", nullptr, nullptr)
OPTION(prefix_1, "/start-lib", start_lib, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Start group of objects treated as if they were in a library", nullptr, nullptr)
OPTION(prefix_1, "/stdcall-fixup:no", stdcall_fixup_no, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/stdcall-fixup", stdcall_fixup, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/stub:", stub, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Specify DOS stub file", nullptr, nullptr)
OPTION(prefix_1, "/subsystem:", subsystem, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Specify subsystem", nullptr, nullptr)
OPTION(prefix_1, "/summary", summary, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/swaprun:cd", swaprun_cd, Flag, INVALID, swaprun, "cd\0", 0, DefaultVis, 0,
       "Make loader run output binary from swap instead of from CD", nullptr, nullptr)
OPTION(prefix_1, "/swaprun:net", swaprun_net, Flag, INVALID, swaprun, "net\0", 0, DefaultVis, 0,
       "Make loader run output binary from swap instead of from network", nullptr, nullptr)
OPTION(prefix_1, "/swaprun:", swaprun, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Comma-separated list of 'cd' or 'net'", nullptr, nullptr)
OPTION(prefix_1, "/thinlto-emit-imports-files", thinlto_emit_imports_files, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Emit .imports files with -thinlto-index-only", nullptr, nullptr)
OPTION(prefix_1, "/thinlto-index-only:", thinlto_index_only_arg, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "-thinlto-index-only and also write native module names to file", nullptr, nullptr)
OPTION(prefix_1, "/thinlto-index-only", thinlto_index_only, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Instead of linking, emit ThinLTO index files", nullptr, nullptr)
OPTION(prefix_1, "/thinlto-object-suffix-replace:", thinlto_object_suffix_replace, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "'old;new' replace old suffix with new suffix in ThinLTO index", nullptr, nullptr)
OPTION(prefix_1, "/thinlto-prefix-replace:", thinlto_prefix_replace, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "'old;new' replace old prefix with new prefix in ThinLTO outputs", nullptr, nullptr)
OPTION(prefix_1, "/threads:", threads, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Number of threads. '1' disables multi-threading. By default all available hardware threads are used", nullptr, nullptr)
OPTION(prefix_1, "/throwingnew", throwingnew, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/timestamp:", timestamp, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Specify the PE header timestamp", nullptr, nullptr)
OPTION(prefix_1, "/time", show_timing, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/tlbid:", tlbid, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/tlbout:", tlbout, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/tsaware:no", tsaware_no, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Create non-Terminal Server aware executable", nullptr, nullptr)
OPTION(prefix_1, "/tsaware", tsaware, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Create Terminal Server aware executable (default)", nullptr, nullptr)
OPTION(prefix_1, "/vctoolsdir:", vctoolsdir, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Set the location of the VC tools", nullptr, nullptr)
OPTION(prefix_1, "/vctoolsversion:", vctoolsversion, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Specify which VC tools version to use", nullptr, nullptr)
OPTION(prefix_1, "/verbose:", verbose_all, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/verbose", verbose, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/version:", version, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Specify a version number in the PE header", nullptr, nullptr)
OPTION(prefix_2, "--version", dash_dash_version, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Display the version number and exit", nullptr, nullptr)
OPTION(prefix_1, "/vfsoverlay:", vfsoverlay, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Path to a vfsoverlay yaml file to optionally look for /defaultlib's in", nullptr, nullptr)
OPTION(prefix_1, "/wholearchive:", wholearchive_file, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Include all object files from this library", nullptr, nullptr)
OPTION(prefix_1, "/wholearchive", wholearchive_flag, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Include all object files from all libraries", nullptr, nullptr)
OPTION(prefix_1, "/winsdkdir:", winsdkdir, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Set the location of the Windows SDK", nullptr, nullptr)
OPTION(prefix_1, "/winsdkversion:", winsdkversion, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Specify which SDK version to use", nullptr, nullptr)
OPTION(prefix_1, "/winsysroot:", winsysroot, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Adds several subdirectories to the library search paths", nullptr, nullptr)
OPTION(prefix_1, "/wrap:", wrap, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "/WX:no", WX_no, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Don't treat warnings as errors (default)", nullptr, nullptr)
OPTION(prefix_1, "/WX", WX, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Treat warnings as errors", nullptr, nullptr)
OPTION(prefix_3, "/??", help_q, Flag, INVALID, help, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
#endif // OPTION

#ifdef SIMPLE_ENUM_VALUE_TABLE

struct SimpleEnumValue {
  const char *Name;
  unsigned Value;
};

struct SimpleEnumValueTable {
  const SimpleEnumValue *Table;
  unsigned Size;
};
static const SimpleEnumValueTable SimpleEnumValueTables[] = {};
static const unsigned SimpleEnumValueTablesSize = std::size(SimpleEnumValueTables);
#endif // SIMPLE_ENUM_VALUE_TABLE


