/*===- TableGen'erated file -------------------------------------*- C++ -*-===*\
|*                                                                            *|
|* Option Parsing Definitions                                                 *|
|*                                                                            *|
|* Automatically generated file, do not edit!                                 *|
|*                                                                            *|
\*===----------------------------------------------------------------------===*/

/////////
// Prefixes

#ifdef PREFIX
#define COMMA ,
PREFIX(prefix_0, {llvm::StringLiteral("")})
PREFIX(prefix_1, {llvm::StringLiteral("-") COMMA llvm::StringLiteral("")})
PREFIX(prefix_3, {llvm::StringLiteral("--") COMMA llvm::StringLiteral("")})
PREFIX(prefix_2, {llvm::StringLiteral("--") COMMA llvm::StringLiteral("-") COMMA llvm::StringLiteral("")})
#undef COMMA
#endif // PREFIX

/////////
// Prefix Union

#ifdef PREFIX_UNION
#define COMMA ,
PREFIX_UNION({
llvm::StringLiteral("-") COMMA llvm::StringLiteral("--") COMMA llvm::StringLiteral("")})
#undef COMMA
#endif // PREFIX_UNION

/////////
// ValuesCode

#ifdef OPTTABLE_VALUES_CODE
#endif
/////////
// Groups

#ifdef OPTION

//////////
// Options

OPTION(prefix_0, "<input>", INPUT, Input, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_0, "<unknown>", UNKNOWN, Unknown, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "-###", _HASH_HASH_HASH, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Print (but do not run) the commands to run for this compilation", nullptr, nullptr)
OPTION(prefix_2, "--allow-multiple-definition", allow_multiple_definition, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Allow multiple definitions", nullptr, nullptr)
OPTION(prefix_2, "--appcontainer", appcontainer, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Set the appcontainer flag in the executable", nullptr, nullptr)
OPTION(prefix_2, "--as-needed", anonymous_6, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_2, "--Bdynamic", Bdynamic, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Link against shared libraries", nullptr, nullptr)
OPTION(prefix_2, "--Bstatic", Bstatic, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Do not link against shared libraries", nullptr, nullptr)
OPTION(prefix_2, "--build-id", anonymous_7, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "-call_shared", alias_Bdynamic_call_shared, Flag, INVALID, Bdynamic, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_2, "--delayload=", delayload_eq, Joined, INVALID, delayload, nullptr, 0, DefaultVis, 0,
       "DLL to load only on demand", nullptr, nullptr)
OPTION(prefix_2, "--delayload", delayload, Separate, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_2, "--demangle", demangle, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Demangle symbol names (default)", nullptr, nullptr)
OPTION(prefix_2, "--disable-auto-image-base", anonymous_8, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_2, "--disable-auto-import", disable_auto_import, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Don't automatically import data symbols from other DLLs without dllimport", nullptr, nullptr)
OPTION(prefix_2, "--disable-dynamicbase", disable_dynamicbase, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Disable ASLR", nullptr, nullptr)
OPTION(prefix_2, "--disable-high-entropy-va", disable_high_entropy_va, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Don't set the 'high entropy VA' flag", nullptr, nullptr)
OPTION(prefix_2, "--disable-no-seh", disable_no_seh, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Don't set the 'no SEH' flag", nullptr, nullptr)
OPTION(prefix_2, "--disable-nxcompat", disable_nxcompat, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Don't set the 'nxcompat' flag", nullptr, nullptr)
OPTION(prefix_2, "--disable-reloc-section", disable_reloc_section, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Disable base relocations", nullptr, nullptr)
OPTION(prefix_2, "--disable-runtime-pseudo-reloc", disable_runtime_pseudo_reloc, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Don't do automatic imports that require runtime fixups", nullptr, nullptr)
OPTION(prefix_2, "--disable-stdcall-fixup", disable_stdcall_fixup, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Don't resolve stdcall/fastcall/vectorcall to undecorated symbols", nullptr, nullptr)
OPTION(prefix_2, "--disable-tsaware", disable_tsaware, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Don't set the 'Terminal Server aware' flag", nullptr, nullptr)
OPTION(prefix_1, "-dn", alias_Bstatic_dn, Flag, INVALID, Bstatic, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_2, "--dynamicbase", dynamicbase, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Enable ASLR", nullptr, nullptr)
OPTION(prefix_1, "-dy", alias_Bdynamic_dy, Flag, INVALID, Bdynamic, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_2, "--enable-auto-image-base", anonymous_9, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_2, "--enable-auto-import", enable_auto_import, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Automatically import data symbols from other DLLs where needed", nullptr, nullptr)
OPTION(prefix_2, "--enable-reloc-section", enable_reloc_section, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Enable base relocations", nullptr, nullptr)
OPTION(prefix_2, "--enable-runtime-pseudo-reloc", enable_runtime_pseudo_reloc, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Allow automatic imports that require runtime fixups", nullptr, nullptr)
OPTION(prefix_2, "--enable-stdcall-fixup", enable_stdcall_fixup, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Resolve stdcall/fastcall/vectorcall to undecorated symbols without warnings", nullptr, nullptr)
OPTION(prefix_2, "--end-group", anonymous_10, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_2, "--entry=", entry_eq, Joined, INVALID, entry, nullptr, 0, DefaultVis, 0,
       "Name of entry point symbol", "<entry>", nullptr)
OPTION(prefix_2, "--entry", entry, Separate, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, "<entry>", nullptr)
OPTION(prefix_3, "--error-limit=", error_limit_eq, Joined, INVALID, error_limit, nullptr, 0, DefaultVis, 0,
       "Maximum number of errors to emit before stopping (0 = no limit)", nullptr, nullptr)
OPTION(prefix_3, "--error-limit", error_limit, Separate, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_2, "--exclude-all-symbols", exclude_all_symbols, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Don't automatically export any symbols", nullptr, nullptr)
OPTION(prefix_2, "--exclude-symbols=", exclude_symbols_eq, Joined, INVALID, exclude_symbols, nullptr, 0, DefaultVis, 0,
       "Exclude symbols from automatic export", "<symbol[,symbol,...]>", nullptr)
OPTION(prefix_2, "--exclude-symbols", exclude_symbols, Separate, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, "<symbol[,symbol,...]>", nullptr)
OPTION(prefix_2, "--export-all-symbols", export_all_symbols, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Export all symbols even if a def file or dllexport attributes are used", nullptr, nullptr)
OPTION(prefix_1, "-e", alias_entry_e, JoinedOrSeparate, INVALID, entry, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_2, "--fatal-warnings", fatal_warnings, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Treat warnings as errors", nullptr, nullptr)
OPTION(prefix_2, "--file-alignment=", file_alignment_eq, Joined, INVALID, file_alignment, nullptr, 0, DefaultVis, 0,
       "Set file alignment", nullptr, nullptr)
OPTION(prefix_2, "--file-alignment", file_alignment, Separate, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_3, "--full-shutdown", anonymous_11, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_2, "--gc-sections", gc_sections, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Remove unused sections", nullptr, nullptr)
OPTION(prefix_2, "--guard-cf", guard_cf, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Enable Control Flow Guard", nullptr, nullptr)
OPTION(prefix_2, "--guard-longjmp", guard_longjmp, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Enable Control Flow Guard long jump hardening (default for --guard-cf)", nullptr, nullptr)
OPTION(prefix_2, "--heap=", heap_eq, Joined, INVALID, heap, nullptr, 0, DefaultVis, 0,
       "Set size of the initial heap", nullptr, nullptr)
OPTION(prefix_2, "--heap", heap, Separate, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_2, "--help", help, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Print option help", nullptr, nullptr)
OPTION(prefix_2, "--high-entropy-va", high_entropy_va, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Set the 'high entropy VA' flag", nullptr, nullptr)
OPTION(prefix_2, "--icf=", icf_eq, Joined, INVALID, icf, nullptr, 0, DefaultVis, 0,
       "Identical code folding", nullptr, nullptr)
OPTION(prefix_2, "--icf", icf, Separate, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_2, "--image-base=", image_base_eq, Joined, INVALID, image_base, nullptr, 0, DefaultVis, 0,
       "Base address of the program", nullptr, nullptr)
OPTION(prefix_2, "--image-base", image_base, Separate, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_2, "--insert-timestamp", insert_timestamp, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Include PE header timestamp", nullptr, nullptr)
OPTION(prefix_2, "--kill-at", kill_at, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Remove @n from exported symbols", nullptr, nullptr)
OPTION(prefix_3, "--large-address-aware", large_address_aware, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Enable large addresses", nullptr, nullptr)
OPTION(prefix_3, "--lto-CGO", lto_CGO, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Codegen optimization level for LTO", "<cgopt-level>", nullptr)
OPTION(prefix_3, "--lto-cs-profile-file=", lto_cs_profile_file, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Context sensitive profile file path", nullptr, nullptr)
OPTION(prefix_3, "--lto-cs-profile-generate", lto_cs_profile_generate, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Perform context sensitive PGO instrumentation", nullptr, nullptr)
OPTION(prefix_3, "--lto-O", lto_O, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Optimization level for LTO", "<opt-level>", nullptr)
OPTION(prefix_1, "-L", L, JoinedOrSeparate, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Add a directory to the library search path", "<dir>", nullptr)
OPTION(prefix_1, "-l", l, JoinedOrSeparate, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Root name of library to use", "<libName>", nullptr)
OPTION(prefix_2, "--major-image-version=", anonymous_12_eq, Joined, INVALID, anonymous_12, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_2, "--major-image-version", anonymous_12, Separate, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_3, "--major-os-version=", major_os_version_eq, Joined, INVALID, major_os_version, nullptr, 0, DefaultVis, 0,
       "Set the OS and subsystem major version", nullptr, nullptr)
OPTION(prefix_3, "--major-os-version", major_os_version, Separate, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_3, "--major-subsystem-version=", major_subsystem_version_eq, Joined, INVALID, major_subsystem_version, nullptr, 0, DefaultVis, 0,
       "Set the OS and subsystem major version", nullptr, nullptr)
OPTION(prefix_3, "--major-subsystem-version", major_subsystem_version, Separate, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_2, "--Map=", map_eq, Joined, INVALID, map, nullptr, 0, DefaultVis, 0,
       "Output a linker map", nullptr, nullptr)
OPTION(prefix_2, "--Map", map, Separate, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_2, "--minor-image-version=", anonymous_13_eq, Joined, INVALID, anonymous_13, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_2, "--minor-image-version", anonymous_13, Separate, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_3, "--minor-os-version=", minor_os_version_eq, Joined, INVALID, minor_os_version, nullptr, 0, DefaultVis, 0,
       "Set the OS and subsystem minor version", nullptr, nullptr)
OPTION(prefix_3, "--minor-os-version", minor_os_version, Separate, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_3, "--minor-subsystem-version=", minor_subsystem_version_eq, Joined, INVALID, minor_subsystem_version, nullptr, 0, DefaultVis, 0,
       "Set the OS and subsystem minor version", nullptr, nullptr)
OPTION(prefix_3, "--minor-subsystem-version", minor_subsystem_version, Separate, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_2, "--mllvm=", mllvm_eq, Joined, INVALID, mllvm, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_2, "--mllvm", mllvm, Separate, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "-m", m, JoinedOrSeparate, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Set target emulation", nullptr, nullptr)
OPTION(prefix_2, "--no-allow-multiple-definition", no_allow_multiple_definition, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Do not allow multiple definitions (default)", nullptr, nullptr)
OPTION(prefix_2, "--no-demangle", no_demangle, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Do not demangle symbol names", nullptr, nullptr)
OPTION(prefix_2, "--no-dynamicbase", alias_no_dynamicbase, Flag, INVALID, disable_dynamicbase, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_2, "--no-fatal-warnings", no_fatal_warnings, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Do not treat warnings as errors (default)", nullptr, nullptr)
OPTION(prefix_2, "--no-gc-sections", no_gc_sections, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Don't remove unused sections", nullptr, nullptr)
OPTION(prefix_2, "--no-guard-cf", no_guard_cf, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Do not enable Control Flow Guard (default)", nullptr, nullptr)
OPTION(prefix_2, "--no-guard-longjmp", no_guard_longjmp, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Do not enable Control Flow Guard long jump hardening", nullptr, nullptr)
OPTION(prefix_2, "--no-insert-timestamp", no_insert_timestamp, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Don't include PE header timestamp", nullptr, nullptr)
OPTION(prefix_2, "--no-seh", no_seh, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Set the 'no SEH' flag in the executable", nullptr, nullptr)
OPTION(prefix_2, "--no-undefined", anonymous_14, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_2, "--no-whole-archive", no_whole_archive, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "No longer include all object files for following archives", nullptr, nullptr)
OPTION(prefix_1, "-non_shared", alias_Bstatic_non_shared, Flag, INVALID, Bstatic, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_2, "--nxcompat", nxcompat, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Set the 'nxcompat' flag in the executable", nullptr, nullptr)
OPTION(prefix_2, "--out-implib=", out_implib_eq, Joined, INVALID, out_implib, nullptr, 0, DefaultVis, 0,
       "Import library name", nullptr, nullptr)
OPTION(prefix_2, "--out-implib", out_implib, Separate, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_2, "--output-def=", output_def_eq, Joined, INVALID, output_def, nullptr, 0, DefaultVis, 0,
       "Output def file", nullptr, nullptr)
OPTION(prefix_2, "--output-def", output_def, Separate, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "-O", anonymous_5, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "-o", o, JoinedOrSeparate, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Path to file to write output", "<path>", nullptr)
OPTION(prefix_2, "--pdb=", pdb_eq, Joined, INVALID, pdb, nullptr, 0, DefaultVis, 0,
       "Output PDB debug info file, chosen implicitly if the argument is empty", nullptr, nullptr)
OPTION(prefix_2, "--pdb", pdb, Separate, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_2, "--pic-executable", anonymous_15, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_2, "--plugin-opt=-fresolution=", anonymous_20, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_2, "--plugin-opt=-pass-through=", anonymous_21, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_2, "--plugin-opt=-", plugin_opt_eq_minus, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Specify an LLVM option for compatibility with LLVMgold.so", nullptr, nullptr)
OPTION(prefix_2, "--plugin-opt=cs-profile-generate", anonymous_2, Flag, INVALID, lto_cs_profile_generate, nullptr, 0, DefaultVis, 0,
       "Alias for --lto-cs-profile-generate", nullptr, nullptr)
OPTION(prefix_2, "--plugin-opt=cs-profile-path=", anonymous_3, Joined, INVALID, lto_cs_profile_file, nullptr, 0, DefaultVis, 0,
       "Alias for --lto-cs-profile-file", nullptr, nullptr)
OPTION(prefix_2, "--plugin-opt=dwo_dir=", plugin_opt_dwo_dir_eq, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Directory to store .dwo files when LTO and debug fission are used", nullptr, nullptr)
OPTION(prefix_2, "--plugin-opt=jobs=", anonymous_4, Joined, INVALID, thinlto_jobs_eq, nullptr, 0, DefaultVis, 0,
       "Alias for --thinlto-jobs=", nullptr, nullptr)
OPTION(prefix_2, "--plugin-opt=mcpu=", plugin_opt_mcpu_eq, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_2, "--plugin-opt=O", anonymous_1, Joined, INVALID, lto_O, nullptr, 0, DefaultVis, 0,
       "Alias for --lto-O", nullptr, nullptr)
OPTION(prefix_2, "--plugin-opt=thinlto", anonymous_0, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_2, "--plugin-opt=", plugin_opt_eq, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_2, "--plugin=", anonymous_16_eq, Joined, INVALID, anonymous_16, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_2, "--plugin", anonymous_16, Separate, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_2, "--reproduce=", reproduce_eq, Joined, INVALID, reproduce, nullptr, 0, DefaultVis, 0,
       "Write a tar file containing input files and command line options to reproduce link", nullptr, nullptr)
OPTION(prefix_2, "--reproduce", reproduce, Separate, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_2, "--require-defined=", require_defined_eq, Joined, INVALID, require_defined, nullptr, 0, DefaultVis, 0,
       "Force symbol to be added to symbol table as an undefined one", nullptr, nullptr)
OPTION(prefix_2, "--require-defined", require_defined, Separate, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_2, "--section-alignment=", section_alignment_eq, Joined, INVALID, section_alignment, nullptr, 0, DefaultVis, 0,
       "Set section alignment", nullptr, nullptr)
OPTION(prefix_2, "--section-alignment", section_alignment, Separate, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_2, "--shared", shared, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Build a shared object", nullptr, nullptr)
OPTION(prefix_2, "--sort-common", anonymous_18, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_2, "--stack=", stack_eq, Joined, INVALID, stack, nullptr, 0, DefaultVis, 0,
       "Set size of the initial stack", nullptr, nullptr)
OPTION(prefix_2, "--stack", stack, Separate, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_2, "--start-group", anonymous_19, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "-static", alias_Bstatic_static, Flag, INVALID, Bstatic, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_2, "--strip-all", strip_all, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Omit all symbol information from the output binary", nullptr, nullptr)
OPTION(prefix_2, "--strip-debug", strip_debug, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Omit all debug information, but keep symbol information", nullptr, nullptr)
OPTION(prefix_2, "--subsystem=", subs_eq, Joined, INVALID, subs, nullptr, 0, DefaultVis, 0,
       "Specify subsystem", nullptr, nullptr)
OPTION(prefix_2, "--subsystem", subs, Separate, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_2, "--sysroot=", anonymous_17_eq, Joined, INVALID, anonymous_17, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_2, "--sysroot", anonymous_17, Separate, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "-S", alias_strip_S, Flag, INVALID, strip_debug, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "-s", alias_strip_s, Flag, INVALID, strip_all, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_3, "--thinlto-cache-dir=", thinlto_cache_dir_eq, Joined, INVALID, thinlto_cache_dir, nullptr, 0, DefaultVis, 0,
       "Path to ThinLTO cached object file directory", nullptr, nullptr)
OPTION(prefix_3, "--thinlto-cache-dir", thinlto_cache_dir, Separate, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_3, "--thinlto-jobs=", thinlto_jobs_eq, Joined, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Number of ThinLTO jobs. Default to --threads=", nullptr, nullptr)
OPTION(prefix_3, "--threads=", threads_eq, Joined, INVALID, threads, nullptr, 0, DefaultVis, 0,
       "Number of threads. '1' disables multi-threading. By default all available hardware threads are used", nullptr, nullptr)
OPTION(prefix_3, "--threads", threads, Separate, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_2, "--tsaware", tsaware, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Set the 'Terminal Server aware' flag", nullptr, nullptr)
OPTION(prefix_2, "--undefined=", undefined_eq, Joined, INVALID, undefined, nullptr, 0, DefaultVis, 0,
       "Include symbol in the link, if available", nullptr, nullptr)
OPTION(prefix_2, "--undefined", undefined, Separate, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_1, "-u", alias_undefined_u, JoinedOrSeparate, INVALID, undefined, nullptr, 0, DefaultVis, 0, nullptr, nullptr, nullptr)
OPTION(prefix_2, "--verbose", verbose, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Verbose mode", nullptr, nullptr)
OPTION(prefix_2, "--version", version, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Display the version number and exit", nullptr, nullptr)
OPTION(prefix_1, "-v", v, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Display the version number", nullptr, nullptr)
OPTION(prefix_2, "--whole-archive", whole_archive, Flag, INVALID, INVALID, nullptr, 0, DefaultVis, 0,
       "Include all object files for following archives", nullptr, nullptr)
OPTION(prefix_2, "--wrap=", wrap_eq, Joined, INVALID, wrap, nullptr, 0, DefaultVis, 0,
       "Use wrapper functions for symbol", "<symbol>", nullptr)
OPTION(prefix_2, "--wrap", wrap, Separate, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, "<symbol>", nullptr)
OPTION(prefix_2, "--Xlink=", Xlink_eq, Joined, INVALID, Xlink, nullptr, 0, DefaultVis, 0,
       "Pass <arg> to the COFF linker", "<arg>", nullptr)
OPTION(prefix_2, "--Xlink", Xlink, Separate, INVALID, INVALID, nullptr, 0, DefaultVis, 0, nullptr, "<arg>", nullptr)
#endif // OPTION

#ifdef SIMPLE_ENUM_VALUE_TABLE

struct SimpleEnumValue {
  const char *Name;
  unsigned Value;
};

struct SimpleEnumValueTable {
  const SimpleEnumValue *Table;
  unsigned Size;
};
static const SimpleEnumValueTable SimpleEnumValueTables[] = {};
static const unsigned SimpleEnumValueTablesSize = std::size(SimpleEnumValueTables);
#endif // SIMPLE_ENUM_VALUE_TABLE


