# Install script for directory: /home/angga/tc-build/src/llvm-project/clang/lib/Headers

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

# Set default install directory permissions.
if(NOT DEFINED CMAKE_OBJDUMP)
  set(CMAKE_OBJDUMP "/usr/lib/llvm-14/bin/llvm-objdump")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xclang-resource-headersx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/clang/18/include" TYPE FILE FILES
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/builtins.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/float.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/inttypes.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/iso646.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/limits.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/module.modulemap"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/stdalign.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/stdarg.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__stdarg___gnuc_va_list.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__stdarg___va_copy.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__stdarg_va_arg.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__stdarg_va_copy.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__stdarg_va_list.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/stdatomic.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/stdbool.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/stddef.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__stddef_max_align_t.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__stddef_null.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__stddef_nullptr_t.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__stddef_offsetof.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__stddef_ptrdiff_t.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__stddef_rsize_t.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__stddef_size_t.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__stddef_unreachable.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__stddef_wchar_t.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__stddef_wint_t.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/stdint.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/stdnoreturn.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/tgmath.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/unwind.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/varargs.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/arm_acle.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/arm_cmse.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/armintr.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/arm64intr.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/arm_neon_sve_bridge.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__clang_cuda_builtin_vars.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__clang_cuda_math.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__clang_cuda_cmath.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__clang_cuda_complex_builtins.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__clang_cuda_device_functions.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__clang_cuda_intrinsics.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__clang_cuda_texture_intrinsics.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__clang_cuda_libdevice_declares.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__clang_cuda_math_forward_declares.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__clang_cuda_runtime_wrapper.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/hexagon_circ_brev_intrinsics.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/hexagon_protos.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/hexagon_types.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/hvx_hexagon_protos.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__clang_hip_libdevice_declares.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__clang_hip_cmath.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__clang_hip_math.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__clang_hip_stdlib.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__clang_hip_runtime_wrapper.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/larchintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/msa.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/opencl-c.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/opencl-c-base.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/altivec.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/htmintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/htmxlintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/riscv_ntlh.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/sifive_vector.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/s390intrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/vecintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/velintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/velintrin_gen.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/velintrin_approx.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/adxintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/ammintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/amxcomplexintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/amxfp16intrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/amxintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx2intrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512bf16intrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512bitalgintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512bwintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512cdintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512dqintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512erintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512fintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512fp16intrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512ifmaintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512ifmavlintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512pfintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512vbmi2intrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512vbmiintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512vbmivlintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512vlbf16intrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512vlbitalgintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512vlbwintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512vlcdintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512vldqintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512vlfp16intrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512vlintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512vlvbmi2intrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512vlvnniintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512vlvp2intersectintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512vnniintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512vp2intersectintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512vpopcntdqintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512vpopcntdqvlintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avxifmaintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avxintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avxneconvertintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avxvnniint16intrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avxvnniint8intrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avxvnniintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/bmi2intrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/bmiintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/cetintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/cldemoteintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/clflushoptintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/clwbintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/clzerointrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/cmpccxaddintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/crc32intrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/emmintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/enqcmdintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/f16cintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/fma4intrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/fmaintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/fxsrintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/gfniintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/hresetintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/ia32intrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/immintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/invpcidintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/keylockerintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/lwpintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/lzcntintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/mm3dnow.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/mmintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/movdirintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/mwaitxintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/nmmintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/pconfigintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/pkuintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/pmmintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/popcntintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/prfchiintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/prfchwintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/ptwriteintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/raointintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/rdpruintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/rdseedintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/rtmintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/serializeintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/sgxintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/sha512intrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/shaintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/sm3intrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/sm4intrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/smmintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/tbmintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/tmmintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/tsxldtrkintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/uintrintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/vaesintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/vpclmulqdqintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/waitpkgintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/wbnoinvdintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__wmmintrin_aes.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/wmmintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__wmmintrin_pclmul.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/x86gprintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/x86intrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/xmmintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/xopintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/xsavecintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/xsaveintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/xsaveoptintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/xsavesintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/xtestintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/cet.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/cpuid.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/wasm_simd128.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/intrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/vadefs.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/mm_malloc.h"
    "/home/angga/tc-build/build/llvm/final/tools/clang/lib/Headers/arm_neon.h"
    "/home/angga/tc-build/build/llvm/final/tools/clang/lib/Headers/arm_fp16.h"
    "/home/angga/tc-build/build/llvm/final/tools/clang/lib/Headers/arm_sve.h"
    "/home/angga/tc-build/build/llvm/final/tools/clang/lib/Headers/arm_sme_draft_spec_subject_to_change.h"
    "/home/angga/tc-build/build/llvm/final/tools/clang/lib/Headers/arm_bf16.h"
    "/home/angga/tc-build/build/llvm/final/tools/clang/lib/Headers/arm_mve.h"
    "/home/angga/tc-build/build/llvm/final/tools/clang/lib/Headers/arm_cde.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xclang-resource-headersx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/clang/18/include/cuda_wrappers" TYPE FILE FILES
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/cuda_wrappers/algorithm"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/cuda_wrappers/cmath"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/cuda_wrappers/complex"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/cuda_wrappers/new"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xclang-resource-headersx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/clang/18/include/cuda_wrappers/bits" TYPE FILE FILES
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/cuda_wrappers/bits/shared_ptr_base.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/cuda_wrappers/bits/basic_string.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/cuda_wrappers/bits/basic_string.tcc"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xclang-resource-headersx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/clang/18/include/ppc_wrappers" TYPE FILE FILES
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/ppc_wrappers/mmintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/ppc_wrappers/xmmintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/ppc_wrappers/mm_malloc.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/ppc_wrappers/emmintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/ppc_wrappers/pmmintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/ppc_wrappers/tmmintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/ppc_wrappers/smmintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/ppc_wrappers/nmmintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/ppc_wrappers/bmiintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/ppc_wrappers/bmi2intrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/ppc_wrappers/immintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/ppc_wrappers/x86intrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/ppc_wrappers/x86gprintrin.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xclang-resource-headersx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/clang/18/include/llvm_libc_wrappers" TYPE FILE FILES
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/llvm_libc_wrappers/assert.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/llvm_libc_wrappers/stdio.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/llvm_libc_wrappers/stdlib.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/llvm_libc_wrappers/string.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/llvm_libc_wrappers/ctype.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/llvm_libc_wrappers/inttypes.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/llvm_libc_wrappers/time.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xclang-resource-headersx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/clang/18/include/openmp_wrappers" TYPE FILE FILES
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/openmp_wrappers/math.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/openmp_wrappers/cmath"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/openmp_wrappers/complex.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/openmp_wrappers/complex"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/openmp_wrappers/__clang_openmp_device_functions.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/openmp_wrappers/complex_cmath.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/openmp_wrappers/new"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xcore-resource-headersx")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/clang/18/include" TYPE FILE FILES
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/builtins.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/float.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/inttypes.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/iso646.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/limits.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/module.modulemap"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/stdalign.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/stdarg.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__stdarg___gnuc_va_list.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__stdarg___va_copy.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__stdarg_va_arg.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__stdarg_va_copy.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__stdarg_va_list.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/stdatomic.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/stdbool.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/stddef.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__stddef_max_align_t.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__stddef_null.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__stddef_nullptr_t.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__stddef_offsetof.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__stddef_ptrdiff_t.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__stddef_rsize_t.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__stddef_size_t.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__stddef_unreachable.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__stddef_wchar_t.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__stddef_wint_t.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/stdint.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/stdnoreturn.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/tgmath.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/unwind.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/varargs.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xarm-common-resource-headersx")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/clang/18/include" TYPE FILE FILES
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/arm_acle.h"
    "/home/angga/tc-build/build/llvm/final/tools/clang/lib/Headers/arm_neon.h"
    "/home/angga/tc-build/build/llvm/final/tools/clang/lib/Headers/arm_fp16.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xarm-resource-headersx")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/clang/18/include" TYPE FILE FILES
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/arm_cmse.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/armintr.h"
    "/home/angga/tc-build/build/llvm/final/tools/clang/lib/Headers/arm_mve.h"
    "/home/angga/tc-build/build/llvm/final/tools/clang/lib/Headers/arm_cde.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xaarch64-resource-headersx")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/clang/18/include" TYPE FILE FILES
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/arm64intr.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/arm_neon_sve_bridge.h"
    "/home/angga/tc-build/build/llvm/final/tools/clang/lib/Headers/arm_sve.h"
    "/home/angga/tc-build/build/llvm/final/tools/clang/lib/Headers/arm_sme_draft_spec_subject_to_change.h"
    "/home/angga/tc-build/build/llvm/final/tools/clang/lib/Headers/arm_bf16.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xcuda-resource-headersx")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/clang/18/include/cuda_wrappers" TYPE FILE FILES
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/cuda_wrappers/algorithm"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/cuda_wrappers/cmath"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/cuda_wrappers/complex"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/cuda_wrappers/new"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xcuda-resource-headersx")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/clang/18/include/cuda_wrappers/bits" TYPE FILE FILES
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/cuda_wrappers/bits/shared_ptr_base.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/cuda_wrappers/bits/basic_string.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/cuda_wrappers/bits/basic_string.tcc"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xcuda-resource-headersx")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/clang/18/include" TYPE FILE FILES
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__clang_cuda_builtin_vars.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__clang_cuda_math.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__clang_cuda_cmath.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__clang_cuda_complex_builtins.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__clang_cuda_device_functions.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__clang_cuda_intrinsics.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__clang_cuda_texture_intrinsics.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__clang_cuda_libdevice_declares.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__clang_cuda_math_forward_declares.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__clang_cuda_runtime_wrapper.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xhexagon-resource-headersx")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/clang/18/include" TYPE FILE FILES
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/hexagon_circ_brev_intrinsics.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/hexagon_protos.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/hexagon_types.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/hvx_hexagon_protos.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xhip-resource-headersx")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/clang/18/include" TYPE FILE FILES
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__clang_hip_libdevice_declares.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__clang_hip_cmath.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__clang_hip_math.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__clang_hip_stdlib.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__clang_hip_runtime_wrapper.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xloongarch-resource-headersx")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/clang/18/include" TYPE FILE FILES "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/larchintrin.h")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xmips-resource-headersx")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/clang/18/include" TYPE FILE FILES "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/msa.h")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xppc-resource-headersx")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/clang/18/include/ppc_wrappers" TYPE FILE FILES
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/ppc_wrappers/mmintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/ppc_wrappers/xmmintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/ppc_wrappers/mm_malloc.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/ppc_wrappers/emmintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/ppc_wrappers/pmmintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/ppc_wrappers/tmmintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/ppc_wrappers/smmintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/ppc_wrappers/nmmintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/ppc_wrappers/bmiintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/ppc_wrappers/bmi2intrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/ppc_wrappers/immintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/ppc_wrappers/x86intrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/ppc_wrappers/x86gprintrin.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xppc-resource-headersx")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/clang/18/include" TYPE FILE FILES "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/altivec.h")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xppc-htm-resource-headersx")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/clang/18/include" TYPE FILE FILES
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/htmintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/htmxlintrin.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xriscv-resource-headersx")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/clang/18/include" TYPE FILE FILES
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/riscv_ntlh.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/sifive_vector.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xsystemz-resource-headersx")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/clang/18/include" TYPE FILE FILES
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/s390intrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/vecintrin.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xve-resource-headersx")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/clang/18/include" TYPE FILE FILES
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/velintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/velintrin_gen.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/velintrin_approx.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xwebassembly-resource-headersx")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/clang/18/include" TYPE FILE FILES "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/wasm_simd128.h")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xx86-resource-headersx")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/clang/18/include" TYPE FILE FILES
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/adxintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/ammintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/amxcomplexintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/amxfp16intrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/amxintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx2intrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512bf16intrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512bitalgintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512bwintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512cdintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512dqintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512erintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512fintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512fp16intrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512ifmaintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512ifmavlintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512pfintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512vbmi2intrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512vbmiintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512vbmivlintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512vlbf16intrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512vlbitalgintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512vlbwintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512vlcdintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512vldqintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512vlfp16intrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512vlintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512vlvbmi2intrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512vlvnniintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512vlvp2intersectintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512vnniintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512vp2intersectintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512vpopcntdqintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avx512vpopcntdqvlintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avxifmaintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avxintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avxneconvertintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avxvnniint16intrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avxvnniint8intrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/avxvnniintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/bmi2intrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/bmiintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/cetintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/cldemoteintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/clflushoptintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/clwbintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/clzerointrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/cmpccxaddintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/crc32intrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/emmintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/enqcmdintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/f16cintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/fma4intrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/fmaintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/fxsrintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/gfniintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/hresetintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/ia32intrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/immintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/invpcidintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/keylockerintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/lwpintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/lzcntintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/mm3dnow.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/mmintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/movdirintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/mwaitxintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/nmmintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/pconfigintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/pkuintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/pmmintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/popcntintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/prfchiintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/prfchwintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/ptwriteintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/raointintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/rdpruintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/rdseedintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/rtmintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/serializeintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/sgxintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/sha512intrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/shaintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/sm3intrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/sm4intrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/smmintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/tbmintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/tmmintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/tsxldtrkintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/uintrintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/vaesintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/vpclmulqdqintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/waitpkgintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/wbnoinvdintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__wmmintrin_aes.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/wmmintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/__wmmintrin_pclmul.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/x86gprintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/x86intrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/xmmintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/xopintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/xsavecintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/xsaveintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/xsaveoptintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/xsavesintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/xtestintrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/cet.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/cpuid.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xhlsl-resource-headersx")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/clang/18/include" TYPE FILE FILES "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/hlsl.h")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xhlsl-resource-headersx")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/clang/18/include/hlsl" TYPE FILE FILES
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/hlsl/hlsl_basic_types.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/hlsl/hlsl_intrinsics.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xopencl-resource-headersx")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/clang/18/include" TYPE FILE FILES
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/opencl-c.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/opencl-c-base.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xopenmp-resource-headersx")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/clang/18/include/openmp_wrappers" TYPE FILE FILES
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/openmp_wrappers/math.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/openmp_wrappers/cmath"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/openmp_wrappers/complex.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/openmp_wrappers/complex"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/openmp_wrappers/__clang_openmp_device_functions.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/openmp_wrappers/complex_cmath.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/openmp_wrappers/new"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xopenmp-resource-headersx")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/clang/18/include/openmp_wrappers" TYPE FILE FILES
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/openmp_wrappers/math.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/openmp_wrappers/cmath"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/openmp_wrappers/complex.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/openmp_wrappers/complex"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/openmp_wrappers/__clang_openmp_device_functions.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/openmp_wrappers/complex_cmath.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/openmp_wrappers/new"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xutility-resource-headersx")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/clang/18/include" TYPE FILE FILES "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/mm_malloc.h")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xwindows-resource-headersx")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/clang/18/include" TYPE FILE FILES
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/intrin.h"
    "/home/angga/tc-build/src/llvm-project/clang/lib/Headers/vadefs.h"
    )
endif()

