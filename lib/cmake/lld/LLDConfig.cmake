# This file allows users to call find_package(LLD) and pick up our targets.



set(LLVM_VERSION 18.0.0)
find_package(LLVM ${LLVM_VERSION} EXACT REQUIRED CONFIG
             HINTS "/home/angga/tc-build/build/llvm/final/./lib/cmake/llvm")

set(LLD_EXPORTED_TARGETS "lldCommon;lld;lldCOFF;lldELF;lldMachO;lldMinGW;lldWasm")
set(LLD_CMAKE_DIR "/home/angga/tc-build/build/llvm/final/lib/cmake/lld")
set(LLD_INCLUDE_DIRS "/home/angga/tc-build/src/llvm-project/lld/include;/home/angga/tc-build/build/llvm/final/tools/lld/include")

# Provide all our library targets to users.
include("/home/angga/tc-build/build/llvm/final/lib/cmake/lld/LLDTargets.cmake")
